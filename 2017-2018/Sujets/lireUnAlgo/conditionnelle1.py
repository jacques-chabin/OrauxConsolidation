def f(valeur):
  if valeur > 100:
    res = "A"
    if valeur > 200:
      res = "B"
    else:
      res = "C"
  else:
    if valeur > 50:
      res="D"
    else:
      res="E"
  return res

print(f(20),
f(100),
f(150),
f(50),
f(51),
f(400))

def g(valeur):
  if valeur > 100:
    res = "A"
  if valeur > 200:
    res = "B"
  else:
    res = "C"
  if valeur > 50:
    res="D"
  else:
    res="E"
  return res

print(g(20),
g(100),
g(150),
g(50),
g(51),
g(400))
