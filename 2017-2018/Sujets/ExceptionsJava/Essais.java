import java.util.List;
import java.util.ArrayList;

class PileVideException extends Exception {}

class PilePleinException extends Exception {}

class PasDeNomException extends Exception {}

class TempsNegatifException extends Exception {}

class Tache{
    private String nom;
    private int tempsDeTraitement;
    
    public Tache(String nom, int temps) throws PasDeNomException, TempsNegatifException
    {
        if (nom==null){
            throw new PasDeNomException();
        }
        if (temps<=0){
            throw new TempsNegatifException();
        }
        this.nom = nom;
        this.tempsDeTraitement=temps;
    }
    
    public String getNom() { return this.nom; }
    
    public int getTemps() { return this.tempsDeTraitement; }  
}


class Pile {
    
    private int quantiteMax;// quantité max de dossier dans le tiroir
    private List<Tache> contenu; // ma collection de chaussettes

    public Pile(int quantite) {
        this.quantiteMax=quantite;
        this.contenu = new ArrayList<>();
    }
    
    public boolean estVide(){
        return this.contenu.isEmpty();
    }
    
    public void ajouter(String nom, int temps) throws PilePleinException, PasDeNomException, TempsNegatifException {
        
        if (this.contenu.size() == this.quantiteMax) throw new PilePleinException();
   
        this.contenu.add(new Tache(nom, temps));
    }
    
    public Tache sommet() throws PileVideException {
        if (this.contenu.size() == this.quantiteMax) throw new PileVideException();
        return this.contenu.get(this.contenu.size()-1);
    }
    
    public void traiterLeTacheAuSommetDeLaPile() throws PileVideException {
        if (this.contenu.size() == this.quantiteMax) throw new PileVideException();
        Tache d =this.contenu.get(this.contenu.size()-1);
        System.out.println("Le dossier "+d.getNom()+ " été traité en "+d.getTemps()+" minutes");
        this.contenu.remove(this.contenu.size()-1);
    }
    
    public void traiterTousLesTaches() {
 
    }
}

class Executable{
    
    public static void main(String[] args) {
        Pile pile = new Pile(5);
        try {
            pile.ajouter("Apprendre le cours sur les Iterateurs", 50);
            pile.ajouter("Manger", 45);
            pile.ajouter("Manger", 45);
            pile.traiterTousLesTaches();
            System.out.println(pile.sommet());
        }
        catch (PilePleinException e)
        {System.out.println("Le main pense que la pile est vide");}
        catch (PileVideException e)
        {System.out.println("Le main pense que la pile est pleine");}
        catch (Exception e)
        {System.out.println("Le main est passé par là");}
        System.out.println("sortie du main");
    }
}
